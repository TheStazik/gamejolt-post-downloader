[Plugin] Cordova File

1.Overview

read, write file, append file, remove file, copy file, move file and get directory entries.

[android, blackberry10, ios, windows8, wp8] [crodova cli] [xdk] [phonegap build service]

2.Change log

3.Install plugin

Now all the native plugins are installed automatically
https://plus.google.com/102658703990850475314/posts/XS5jjEApJYV

4.Server setting

not needed

5.API

//actions
Write text
Append text
Read text
Remove file
Copy file
Move file
Get directory entries

//events
On write text succeeded
On write text failed
On append text succeeded
On append text failed
On read text succeeded
On read text failed
On remove file succeeded
On remove file failed
On copy file succeeded
On copy file failed
On move file succeeded
On move file failed
On get directory entries succeeded
On get directory entries failed

//expressions
Text
DirectoryEntriesCount
IsFileAt
IsDirectoryAt
NameAt
FullPathAt

6.Examples

example capx are included in doc folder

7.Test

8.Useful links

Plugins For Cordova
http://cranberrygame.github.io?referrer=readme.txt
