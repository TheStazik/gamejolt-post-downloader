// Создать пустое меню
var menu = new gui.Menu();
// Добавить в него пункты или разделители
menu.append(new gui.MenuItem({ label: 'Ничего не делать' }));
menu.append(new gui.MenuItem({ type: 'separator' }));
// .. и повесить на них обработчики
menu.append(new gui.MenuItem({
  label: 'Сказать "Привет!"',
  click: function() {
    alert('Привет!')
  }
}));

// Показывать в виде контекстного меню
document.body.addEventListener('contextmenu', function(e) { 
  e.preventDefault();
  // В том месте, где мы кликнули
  menu.popup(e.x, e.y);
  return false;
}, false);