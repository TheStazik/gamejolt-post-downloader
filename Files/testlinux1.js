var nwGui = require('nw.gui')
    , nwShell = nwGui.Shell
    , child_process = require('child_process')
    , exec = child_process.exec
    , execSync = child_process.execSync
    , execFile = child_process.execFile
    , execFileSync = child_process.execFileSync
;

var output = execSync("bash /path/to/test.sh");
console.log(output);