const { showMessageBox } = require("nwjs-dialog");
showMessageBox({
  title: "Input Options",
  inputOptions: {
    radios: [
      { label: "Radio 1" },
      { label: "Radio 2", value: true, description: "Description" },
    ],
    checkboxes: [
      { label: "Checkbox 1", description: "Description" },
      { label: "Checkbox 2", value: true },
    ],
    inputs: [
      {
        label: "Input 1",
        placeholder: "Placeholder",
        description: "Description",
      },
      {
        label: "Input 2",
        value: "123456",
        password: true,
        description: "Password Input",
      },
    ],
  },
}).then(({ button, inputData }) => {
  if (button === 0) {
    const { radios, checkboxes, inputs } = inputData;
    console.log({ radios, checkboxes, inputs });
  }
});